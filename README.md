# ART Metadata Convert

A collection of simple scripts that create ART compatible .xmp files from Raw Therapee (.pp3) or Lightroom (.xmp) files.

Currently only grabbing the rating value as the colour, crop, sharpening values are vastly different between apps.

## Getting started

1) Copy the script to your computer
2) Paste the address to the directory of the .pp3 files into the quotes where it says path = r'/'
3) Run the script.

## Caveat

I know the creation of the .xmp file is a dirty way to do it. I just wanted a quick solution, and did not feel like learning and including the xmp module.