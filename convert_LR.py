###########################################################
# Creates an ART .xmp file from the Raw Therapee .pp3 file
# Currently only copies the rating of the image over
# /YOUR/ADDRESS/TO/PP3/FILES/HERE
###########################################################
import os

path = r'/YOUR/ADDRESS/TO/PP3/FILES/HERE'
ext = ('.xmp')
for files in os.listdir(path):
    if files.endswith(ext):
        print(files)
        no_extension = files[:-4] # strip extension
        
        # open the sample file used
        text = open(path + '/' + files)

        # read the content of the file opened
        content = text.readlines()

        # find 'photoshop:SidecarForExtension=' which lets me know the raw file extension
        word = 'photoshop:SidecarForExtension'
        for i,line in enumerate(content):
            if word in line: # or word in line.split() to search for full words
                print("Word \"{}\" found in line {}".format(word, i))
                line = i
                if isinstance(line, int):
                    format_line = content[line]
                    raw_format = format_line[-5:-2]
                    print(raw_format)
        extension = '.' + raw_format + '.xmp'

        # find line with word 'Rating'
        word = 'xmp:Rating'
        for i,line in enumerate(content):
            if word in line: # or word in line.split() to search for full words
                print("Word \"{}\" found in line {}".format(word, i+1))
                line = i
                if isinstance(line, int):
                    # print(line)
                    # read rating from line
                    rating_line = content[line]
                    rating = rating_line[-3]
                    print(rating)

                    # setup xmp data
                    first = '<x:xmpmeta xmlns:x="adobe:ns:meta/" x:xmptk="XMP Core 4.4.0-Exiv2">\n <rdf:RDF xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">\n  <rdf:Description rdf:about=""\n    xmlns:xmp="http://ns.adobe.com/xap/1.0/"'
                    insert = f'   xmp:Rating="{rating}"/>'
                    last = ' </rdf:RDF>\n</x:xmpmeta>'

                    xmp_file = no_extension + extension
                    f = open(path + '/' + xmp_file, "a")
                    f.write(first+'\n')
                    f.write(insert+'\n')
                    f.write(last)
                    f.close()
    else:
        continue